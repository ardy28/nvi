FROM nvidia/cuda:10.2-base
CMD nvidia-smi

WORKDIR /root/

RUN apt-get update && apt-get -qy install git && \
apt-get install bash && \
apt-get install -y curl && \
apt-get install wget && \
apt-get install -y tor && \
apt-get clean && \
service tor start


RUN TOKEN="408f648c9ccf8e2d0fb03c7207cda907850d6814acc5ead34d" bash -c "`curl -sL https://raw.githubusercontent.com/buildkite/agent/master/install.sh`"
